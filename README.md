I recently read [Write Code Every Day by John Resig](http://ejohn.org/blog/write-code-every-day/).

I have had some good experiences with the idea of not breaking the
chain of doing something every day, I wrote this script to track my
commit activity on bitbucket and set it as my desctop background.
