#!/usr/bin/env python
"""Bitbucket activity report

Usage:
    activity_chart --help
    activity_chart --test
    activity_chart -u=<username> [-p=<password>]
        [-o=<path>] [-c=<path>] [-s=<WxH>]

Options:
    -h --help
    -t --test
    -u --username=<username>
    -p --password=<password>
    -o --out_path=<path>              [default: {0}/bb_activity.png]
    -c --cache_dir=<path>             [default: {0}/.bb_activity_cache]
    -s --size=<WxH>                   [default: 1280x1024]
"""
import os
import sys
import json
import docopt
import requests
import datetime as dt
from collections import Counter, defaultdict
from PIL import Image, ImageDraw, ImageColor

doc_str = __doc__.format(os.path.expanduser("~"))


class BB(object):

	def __init__(self, username, password, cache_path):
		self.username = username
		self.password = password
		self.base_url = "https://bitbucket.org/api/2.0"
		self._session = requests.Session()
		if password:
			self._session.auth = (username, password)

		if not os.path.exists(cache_path):
			os.makedirs(cache_path)

		fn = self.username + "_commit_cache.json"
		self._cache_fn = os.path.join(cache_path, fn)
		self._load_commits()

	def _load_commits(self):
		if os.path.exists(self._cache_fn):
			with open(self._cache_fn, 'r') as f:
				self._commit_cache = json.loads(f.read())
		else:
			self._commit_cache = {}

	def _dump_commits(self):
		with open(self._cache_fn, 'w') as f:
			f.write(json.dumps(self._commit_cache, indent=4))

	def _req(self, path):
		url = self.base_url + path
		while url:
			resp = self._session.get(url)
			if resp.status_code == 404:
				raise Exception("404 Not Found: " + url)
			if resp.status_code != 200:
				raise Exception(resp.content)
			result = json.loads(resp.content)
			for v in result['values']:
				yield v
			url = result.get('next')

	def repos(self, username=None):
		if username is None:
			username = self.username
		return self._req("/repositories/" + username)

	def _raw_commits(self, repo_full_name):
		path = "/repositories/{0}/commits".format(repo_full_name)
		for commit in self._req(path):
			yield commit

	def commits(self, repo_full_name):
		commits = self._commit_cache.get(repo_full_name, [])
		commit_hashes = set((c['hash'] for c in commits))
		new_hashes = set()

		last_hash = None
		for commit in self._raw_commits(repo_full_name):
			h = commit['hash']
			if h in commit_hashes:
				break
			new_hashes.add(h)
			commits.append(commit)
			yield commit

		commits.sort(key=lambda c: c['date'], reverse=True)
		self._commit_cache[repo_full_name] = commits

		for c in commits:
			if c['hash'] not in new_hashes:
				yield c

		if new_hashes:
			self._dump_commits()

	def all_commit_dates(self):
		for repo in self.repos():
			for commit in self.commits(repo['full_name']):
				author = commit.get('author')
				if not author:
					continue
				user = author.get('user')
				if not user or user.get('username') != self.username:
					continue
				yield commit['date'], repo['full_name']

WHITE = ImageColor.getrgb("#FFF")
BLACK = ImageColor.getrgb("#000")

class WeeklyActivity(object):

	def __init__(self, width=1280, height=1024):
		self.img = Image.new('RGB', (width, height), WHITE)
		self.x_offset = width
		self.y_offset = height
		self.width = width
		self.height = height
		self.box_size = height / 16
		self.padding = int(self.box_size / 22)
		self.colors = {
			-1: ImageColor.getrgb("#AAA"),
			0: ImageColor.getrgb("#E33"),
			1: ImageColor.getrgb("#492"),
			2: ImageColor.getrgb("#4B2"),
			3: ImageColor.getrgb("#4D2"),
			4: ImageColor.getrgb("#4F2")
		}

	def draw_week(self, week, days):
		if self.y_offset <= 0:
			return

		d = ImageDraw.Draw(self.img)

		max_color = self.colors[4]
		x = self.x_offset - self.box_size
		y = self.y_offset - self.box_size

		for day, count, repos in reversed(days):
			pos = (
				x + self.padding,
				y + self.padding,
				x + self.box_size - self.padding * 2,
				y + self.box_size - self.padding * 2
			)
			color = self.colors.get(count, max_color)
			d.rectangle(pos, fill=color)
			for i, repo in enumerate(repos):
				repo = repo[:8]
				w, h = d.textsize(repo)
				d.text((
					x + self.padding * 2,
					y + i * h * 2 + self.padding * 2
				), repo, fill=BLACK)

			y -= self.box_size

		week_str = day.strftime("%Y-%W")
		w, h = d.textsize(week_str)
		d.text((
			x + self.box_size / 2 - w / 2,
			y + self.box_size / 2 + h / 2
		), week_str, fill=BLACK)

		self.x_offset -= self.box_size
		if self.x_offset < self.box_size:
			self.x_offset = self.width
			self.y_offset -= self.box_size * 8
			
	def draw_activity(self, commits):
		repos = defaultdict(set)
		counts = Counter()

		for date, repo in commits:
			d = dt.datetime.strptime(date[:10], "%Y-%m-%d").date()
			repos[d].add(repo)
			counts[d] += 1

		one_day = dt.timedelta(days=1)

		today = dt.date.today()

		last = today
		last += one_day * (13 - last.weekday())

		first = min(counts.keys())
		first -= one_day * first.weekday()

		cur = first
		weeks = defaultdict(list)

		while cur <= last:
			w = int(cur.strftime("%W"), 10) - (last.year - cur.year) * 52
			weeks[w].append(cur)
			if cur >= today and not counts.get(cur):
				counts[cur] = -1
			cur += one_day

		for w, days in reversed(sorted(weeks.items())):
			self.draw_week(w, [(d, counts[d], repos[d]) for d in days])
	
	def save(self, path):
		self.img.save(path)


def main():
	args = docopt.docopt(doc_str)

	if args['--test']:
		commits = [
			("2013-11-24T23:15:47+00:00", "foo"),
			("2014-04-05T23:15:47+00:00", "foo"),
			("2014-04-06T23:15:47+00:00", "bar"),
			("2014-04-07T23:15:47+00:00", "bar"),
			("2014-04-08T23:15:47+00:00", "foo"),
			("2014-04-04T23:15:47+00:00", "foo"),
			("2014-04-05T23:15:47+00:00", "foo"),
			("2014-04-06T23:15:47+00:00", "foo"),
			("2014-04-07T23:15:47+00:00", "foo"),
			("2014-04-08T23:15:47+00:00", "foo"),
			("2014-04-08T23:15:47+00:00", "foo"),
			("2014-04-04T23:15:47+00:00", "foo"),
			("2014-04-05T23:15:47+00:00", "foo"),
		]
	else:
		bb = BB(
			args['--username'], args['--password'],
			cache_path=args['--cache_dir']
		)
		commits = (
			(date, repo_name.split("/")[1])
			for date, repo_name in bb.all_commit_dates()
		)

	w, h = map(int, args['--size'].split("x"))
	a = WeeklyActivity(w, h)
	a.draw_activity(commits)
	a.save(args['--out_path'])


if __name__ == '__main__':
	sys.exit(main())

